package ua.khpi.oop.gorban02;

import java.util.Random;

/**
 * Завдання: 5. Перевірити, чи є задане число простим (тобто не ділиться
 * без залишку на жодні числа, крім себе і 1).
 *
 * @author Gorban K.
 */

public class Main {
    /**
     * Точка входу консольної програми.
     *
     * @param args параметри командного рядка
     */
    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[10];

        for (int i = 0; i < arr.length; i++)
            arr[i] = (random.nextInt() & Integer.MAX_VALUE) % 100;

        printIsItNaturalNumber(arr);
    }

    /**
     * Друк числа. Функція друкує число та вказує чи є воно простим.
     * @param arr масив чисел.
     */
    private static void printIsItNaturalNumber(int[] arr) {
        for (int i : arr) {
            if (checkIsItANaturalNumber(i))
                System.out.println(i + " is a natural number.");
            else
                System.out.println(i + " is not a natural number.");
        }
    }

    /**
     * Функція перевіряє чи є число простим.
     * @param number число, котре потрібно перевірити.
     * @return true - якщо число просте, інакше - false.
     */
    private static boolean checkIsItANaturalNumber(int number) {
        int currNum = 2;

        if (number > 1) {
            while (number % currNum != 0)
                currNum++;
        }

        return currNum == number;
    }
}