package ua.khpi.oop.gorban08;

import java.util.StringJoiner;

/**
 * Клас 5. Довідник покупця.
 * Торгівельна точка:
 *  - назва;
 *  - адреса;
 *  - телефони (кількість не обмежена);
 *  - спеціалізація;
 *  - час роботи (з зазначенням днів тижня).
 */

public class BuyersGuide {
    private final String name;
    private final String address;
    private final String[] phones;
    private final String specialization;
    private final String workTime;

    public BuyersGuide(){
        this.name = "Комп'ютерний всесвіт";
        this.address = "м. Харків, вул. Гончаренка, 20А";
        this.phones = new String[] { "380661245789", "380673908547" };
        this.specialization = "техніка";
        this.workTime = "Пн-Пт 08.00-18.00 Сб - 09.00-15.00 Нд - вихідний";
    }

    public BuyersGuide(String name, String address, String[] phones, String specialization, String workTime) {
        this.name = name;
        this.address = address;
        this.phones = phones;
        this.specialization = specialization;
        this.workTime = workTime;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String[] getPhones() {
        return phones;
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getWorkTime() {
        return workTime;
    }

    public String toString() {
        return  "Назва: " + name +
                "\nАдреса: " + address +
                "\nТелефони: " + getStringPhones() +
                "\nСпеціалізація: " + specialization +
                "\nЧас роботи: " + workTime;
    }

    /**
     * Функція перетворює масив в рядок, у якому перераховує
     * елементи через кому.
     * @return перетворенний рядок.
     */
    private String getStringPhones() {
        StringJoiner sj = new StringJoiner(", ");
        for (String phone : phones)
            sj.add(phone);

        return sj.toString();
    }
}
