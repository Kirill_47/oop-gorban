package ua.khpi.oop.gorban08;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class XMLCreator {

    /**
     * Функція записує весь довідник в xml файл.
     * @param guide довідник
     */
    static public void encode(List<BuyersGuide> guide){
        DocumentBuilder builder;

        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }  catch (Exception e) {
            System.out.println("Не вдалося зберегти\n");
            return;
        }
        
        Document document = builder.newDocument();

        Element root = document.createElement("root");
        document.appendChild(root);

        for (BuyersGuide item : guide) {
            Element trading_point = document.createElement("trading_point");
            root.appendChild(trading_point);

            Element name = document.createElement("name");
            Element address = document.createElement("address");
            Element phones = document.createElement("phones");
            Element specialization = document.createElement("specialization");
            Element workTime = document.createElement("workTime");

            Text text = document.createTextNode(item.getName());
            trading_point.appendChild(name);
            name.appendChild(text);

            text = document.createTextNode(item.getAddress());
            trading_point.appendChild(address);
            address.appendChild(text);

            String temp = "";
            for (int j = 0; j < item.getPhones().length; j++) {
                temp = temp.concat(item.getPhones()[j]);
                if (j != item.getPhones().length - 1) temp = temp.concat(",");
            }

            text = document.createTextNode(temp);
            trading_point.appendChild(phones);
            phones.appendChild(text);

            text = document.createTextNode(item.getSpecialization());
            trading_point.appendChild(specialization);
            specialization.appendChild(text);

            text = document.createTextNode(item.getWorkTime());
            trading_point.appendChild(workTime);
            workTime.appendChild(text);
        }

        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(document), new StreamResult(new FileOutputStream("save.xml")));
            System.out.println("Вдало збережено\n");
        } catch (Exception e) {
            System.out.println("Не вдалось зберегти\n");
        }
    }

    /**
     * Функція перетворює дані з xml файлу в список торгівельних точок.
     * @return довідник.
     */
    public static List<BuyersGuide> decode() {
        List<BuyersGuide> root = new ArrayList<>();
        File file = new File("save.xml");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            Document doc = dbf.newDocumentBuilder().parse(file);
            Node rootNode = doc.getFirstChild();
            NodeList tradingPoints = rootNode.getChildNodes();
            for (int i = 0; i < tradingPoints.getLength(); i++){
                if (tradingPoints.item(i).getNodeType() == Node.ELEMENT_NODE){
                    NodeList fields = tradingPoints.item(i).getChildNodes();
                    root.add(generate(fields));
                }
            }
            System.out.println("Вдало завантажено\n");
        }  catch (Exception e) {
            System.out.println("Не вдалося завантажити\n");
        }

        return root;
    }

    /**
     * Функція генерує торгівельну точку зі списку вузлів
     * @param fields список вузлів
     * @return торгівельну точку
     */
    private static BuyersGuide generate(NodeList fields) {
        String[] array = new String[0];
        String[] temp = new String[5];
        int j = 0;
        for (int i = 0; i < fields.getLength(); i++) {
            if (fields.item(i).getNodeType() == Node.ELEMENT_NODE) {
                if (!fields.item(i).getNodeName().equals("phones"))
                    temp[j++] = fields.item(i).getFirstChild().getNodeValue();
                else
                    array = fields.item(i).getFirstChild().getNodeValue().split(",");
            }
        }

        return new BuyersGuide(temp[0],temp[1],array,temp[2],temp[3]);
    }
}
