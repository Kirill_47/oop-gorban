package ua.khpi.oop.gorban08;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Menu {
    static List<BuyersGuide> guide = new ArrayList<>();

    /**
     * Функція виконує роботу взаємодії з користувачем
     */
    public static void startMenu() {
        boolean isOver = false;
        while (!isOver){
            printMenu();
            int command = getCommand();
            isOver = doCommand(command);
        }
    }

    /**
     * Функція друкує меню взаємодії з користувачем
     */
    private static void printMenu() {
        System.out.println("\nМеню:");
        System.out.println("1. Додати торгівельну точку");
        System.out.println("2. Видалити торгівельну точку");
        System.out.println("3. Усі торгівельні точки");
        System.out.println("4. Сортування");
        System.out.println("5. Зберегти");
        System.out.println("6. Завантажити");
        System.out.println("7. Завершити роботу");
    }

    /**
     * Функція запитує користувача, котру команду він хоче виконати
     * @return номер команди
     */
    private static int getCommand() {
        Scanner in = new Scanner(System.in);

        System.out.print("Відповідь: ");
        int choose = in.nextInt();
        System.out.println();

        return choose;
    }

    /**
     * Функція виконує задану команду
     * @param command команда
     * @return чи вибрав користувач завершення виконання програми,
     * якщо так - повертає true, інакше - false
     */
    private static boolean doCommand(int command) {
        switch (command){
            case 1:
                addTradingPoint();
                break;
            case 2:
                removeTradingPoint();
                break;
            case 3:
                printGuide();
                break;
            case 4:
                guide.sort(Comparator.comparing(BuyersGuide::getName));
                break;
            case 5:
                XMLCreator.encode(guide);
                break;
            case 6:
                guide = XMLCreator.decode();
                break;
            case 7:
                return true;
            default:
                System.out.println("Невірний пункт меню.");
        }

        return false;
    }

    /**
     * Додавання торгівельну точку в довідник. Функція запитує дані у
     * користувача та додає точку у кінець списку.
     */
    private static void addTradingPoint() {
        Scanner in = new Scanner(System.in);

        System.out.print("Введіть назву: ");
        String name = in.nextLine();
        System.out.print("Введіть адресу: ");
        String address = in.nextLine();
        System.out.print("Введіть номери телефонів: ");
        String[] phones = in.nextLine().split(",");
        System.out.print("Введіть спеціалізацію: ");
        String specialization = in.nextLine();
        System.out.print("Введіть час роботи: ");
        String workTime = in.nextLine();
        guide.add(new BuyersGuide(name, address, phones, specialization, workTime));
    }

    /**
     * Видалення торгівельної точки. Функція запитує у користувача номер точки,
     * котру потрібно видалити.
     */
    private static void removeTradingPoint() {
        Scanner in = new Scanner(System.in);

        if (guide.size() == 0) {
            System.out.println("Торгівельні точки відсутні");
            return;
        }

        int pos;
        do {
            System.out.print("Оберіть номер торгівельної точки: ");
            pos = in.nextInt();
        } while (pos < 1 || pos > guide.size());
        pos--;

        guide.remove(pos);
    }

    /**
     * Функція друкує довідник, перераховуючи усі торгівельні точки.
     */
    private static void printGuide() {
        if (guide.size() == 0) {
            System.out.println("Торгівельні точки відсутні");
            return;
        }

        System.out.println("-----------------------------------------");
        for (int i = 0; i < guide.size(); i++) {
            System.out.println("Торгівельна точка №" + (i + 1) + ": ");
            System.out.println(guide.get(i));
            System.out.println("-----------------------------------------");
        }
    }
}
