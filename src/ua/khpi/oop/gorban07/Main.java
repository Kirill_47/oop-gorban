package ua.khpi.oop.gorban07;

import java.util.ArrayList;
import java.util.List;

/**
 * Завдання: Використання об'єктно-орієнтованого підходу для розробки
 * об'єкта предметної (прикладної) галузі.
 * 5. Довідник покупця
 * Торгівельна точка: назва; адреса; телефони (кількість не обмежена);
 * спеціалізація; час роботи (з зазначенням днів тижня).
 *
 * @author Gorban K.
 */

public class Main {
    /**
     * Точка входу консольної програми.
     *
     * @param args параметри командного рядка
     */
    public static void main(String[] args) {
        List<BuyersGuide> library = new ArrayList<>();

        library.add(new BuyersGuide());
        library.add(new BuyersGuide("Удивительная Мисс",
                             "м. Запоріжжя, пр. Соборний, 150",
                                     new String[] { "380663556874" },
                         "одяг",
                            "Пн-Пт 08.00-17.00 Сб-Нд вихідний"));

        library.add(new BuyersGuide("Roshen",
                "м. Київ, бул. Шевченка, 49",
                new String[] { "380993405689", "380953545785", "380675768546" },
                "їжа",
                "Пн-Пт 08.00-20.00 Сб-Нд 09.00-18.00"));

        for (int i = 1; i <= library.size(); i++) {
            System.out.println("Торгівельна точка №" + i);
            System.out.println(library.get(i - 1));
            System.out.println();
        }
    }
}
